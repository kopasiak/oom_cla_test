
#!/bin/bash

# TODO find a better way to detect this
if [ $CI_COMMIT_BRANCH == master ];
then
	# We are not running on a merge request so let's check single commit
	./ci_scripts/check_cla.sh `git show --format=%ae -s`
else
	# We are running on a merge request so we check all commits that are supposed to be merged
	COMMON_ANCESTOR=`git merge-base HEAD origin/master`
	AUTHORS=`git log --format=%ae $COMMON_ANCESTOR..HEAD | sort | uniq`
	for AUTHOR in $AUTHORS
	do
		./ci_scripts/check_cla.sh $AUTHOR || exit $?
	done
fi
