#!/bin/bash

COMMIT_AUTHOR_EMAIL=$1

CLA_GROUPS="saml/onap-cla-ccla saml/onap-cla-icla"

CLA_LIST=""

for GROUP in $CLA_GROUPS
do
	QUERRY_RESULT=`ssh -i $GERRIT_SSH_KEY -o StrictHostKeyChecking=no -p 29418 $GERRIT_USER_NAME@gerrit.onap.org gerrit ls-members $GROUP`
	RET=$?
	if (( $RET != 0 )) ; then
		echo "Unable to querry onap gerrit"
		exit $RET
	fi
	GROUP_MEMBERS_EMAIL=`echo "$QUERRY_RESULT" | rev | cut -f 1 | rev | tail -n +2`
	CLA_LIST=`echo -e "$CLA_LIST\n$GROUP_MEMBERS_EMAIL" | sort | uniq | awk 'NF'`
done

RES=`echo "$CLA_LIST" | grep $COMMIT_AUTHOR_EMAIL`

if [ -z "$RES" ]; then
	echo "Author $COMMIT_AUTHOR_EMAIL has not been found in any of $CLA_GROUPS gerrit groups"
	exit 127
else
	echo "Author $COMMIT_AUTHOR_EMAIL is approved under ONAP CLA"
	exit 0
fi
